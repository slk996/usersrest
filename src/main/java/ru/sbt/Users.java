package ru.sbt;

import  java.util.*;

public class Users {
    private Map<String, User> users = new HashMap<>();


    Users(){

        users.put("1", new User("1", "Иван", "2name", "login1", "qwerty1"));
        users.put("2", new User("2", "Пётр", "2name", "login2", "qwerty2"));
        users.put("3", new User("3", "Олег", "2name", "login3", "qwerty3"));
    }

    public User get(String id) {
        return users.get(id);
    }

    public List<User> getUsersList(){
        List<User> list = new ArrayList<User>(users.values());
        return list;
    }

    public void addUser(String newId, String newName, String newSurName, String newLogin, String newPass){
        users.put("4", new User(newId, newName, newSurName, newLogin, newPass));
    }


}
