package ru.sbt.request;

public class AddUserRequest {

    private String id;
    private String firstName;
    private String surName;
    private String login;
    private String password;

    public AddUserRequest(String id, String firstName, String surName, String login, String password) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.login = login;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
