package ru.sbt;


import org.springframework.web.bind.annotation.*;
import ru.sbt.request.AddUserRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import java.util.List;
import java.util.UUID;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RestController
//@Consumes({"application/json"})
public class UserController {

    Users users = new Users();

    @RequestMapping("/get")
    public User getUser(@QueryParam("id") String id) {
        return users.get(id);
    }

    @RequestMapping("/users")
    public List<User> getUsers(){
        return users.getUsersList();
    }



    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = APPLICATION_JSON, produces = APPLICATION_JSON)
    @ResponseBody
    public void addUser(@RequestBody User request){

        System.out.println("req " + request.getId() +" "+ request.getFirstName()+" "+request.getSurName()+" "+request.getLogin()+" "+request.getPassword());
//        UUID.randomUUID().toString()

        users.addUser(request.getId(), request.getFirstName(), request.getSurName(), request.getLogin(), request.getPassword());
//

    }


}
